import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const Header = () => {
  return (
    <View style={header}>
      <Text style={headerText}>Cryptocurrency tracker app</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  header: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,
    height: 50,
    backgroundColor: '#cccccc'
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold'
  }
})

const { header, headerText } = styles;

export default Header;